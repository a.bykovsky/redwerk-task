jQuery(document).ready(function ($) {

    var meta_image_frame, btn, meta_image_preview, meta_image, media_attachment;

    $('.image-custom-upload').click(function (e) {
        
        e.preventDefault();
        
        btn = $(this);

        if (meta_image_frame) {
            meta_image_frame.open();
            return;
        }

        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            button: { text: 'Select image' },
        });

        meta_image_frame.on('select', function () {

            meta_image_preview = btn.parents('.twentytwentyonechild-uploader').find('.image-preview');
            meta_image = btn.parents('.twentytwentyonechild-uploader').find('.meta-image');
            meta_image_delete = btn.parents('.twentytwentyonechild-uploader').find('.image-delete');
           
            media_attachment = meta_image_frame.state().get('selection').first().toJSON();
          
            meta_image.val(media_attachment.url);
            meta_image_preview.children('img').attr('src', media_attachment.url);
            meta_image_preview.css('display', 'block');
            meta_image_delete.css('display', 'block');

        });

        meta_image_frame.open();

    });

    $('.image-delete').click(function (e) {
    	
    	e.preventDefault();

    	meta_image_preview = $('.twentytwentyonechild-uploader').find('.image-preview');
    	meta_image = $('.twentytwentyonechild-uploader').find('.meta-image');
    	meta_image_delete = $('.twentytwentyonechild-uploader').find('.image-delete');

    	meta_image.val('');
    	meta_image_preview.children('img').attr('src', '');
    	meta_image_preview.css('display', 'none');
    	meta_image_delete.css('display', 'none');

    });

});