(function ($) {

	initLoadMore();

	function initLoadMore(){

		var btnLoad, currentPage, maxPage;
		var loader = $('.rw_olx-posts .rw_olx_loader');

		$('.rw_olx-load-more a').click(function (e) {
			e.preventDefault();

			btnLoad = $(this);
			currentPage = parseInt( btnLoad.attr('data-current-page') );
			maxPage = parseInt( btnLoad.attr('data-max-pages') );

			var loadData = {
				'action' : 'loadmore_rw_olx',
				'security': params.ajax_nonce,
				'maxPage' : maxPage,
				'currentPage' : currentPage
			};

			$.ajax({
				url: params.ajaxurl,
				data: loadData,
				type: 'POST',
				beforeSend: function(){
					loader.toggle();
					$('.rw_olx-load-more').toggle();
				},
				success: function(data){
					if(data){

						if( $(data) ){
							$('.rw_olx-posts .rw_olx-posts--holder').append($(data));
						}

						if( maxPage > currentPage + 1 ){
							$('.rw_olx-load-more').toggle();
							btnLoad.attr('data-current-page', currentPage + 1);
						}
						
						loader.toggle();

					}
				}
			});
		})
	}

})(jQuery);