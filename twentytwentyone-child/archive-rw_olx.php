<?php get_header(); ?>

<?php global $wp_query; ?>

<?php if ( have_posts() ) : ?>

	<header class="page-header alignwide">
		<h1 class="page-title"><?php _e( 'Публікації', 'twentytwentyonechild' ); ?></h1>
	</header>

	<section class="rw_olx-posts">
		<div class="container">

			<div class="rw_olx-posts--holder">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'blocks/rw-olx-item' ); ?>
				<?php endwhile; ?>
			</div>

			<div class="rw_olx_loader">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/black-loader.gif" alt="Loader">
			</div>

			<?php if( $wp_query->max_num_pages > 1 ): ?>
				<div class="rw_olx-load-more">
					<a href="#" data-max-pages="<?php echo $wp_query->max_num_pages; ?>" data-current-page="1">
						<?php _e('Завантажити ще', 'twentytwentyonechild'); ?>
					</a>
				</div>
			<?php endif ; ?>

		</div>
	</section>

<?php else : ?>

	<?php get_template_part( 'template-parts/content/content-none' ); ?>

<?php endif; ?>

<?php get_footer(); ?>
