<article class="rw_olx-item" id="post-<?php the_ID(); ?>">

	<?php  
		$rw_olx_types = get_the_terms( $post->ID, 'rw_olx_type' );
		$rw_olx_image = get_post_meta($post->ID, 'rw_olx_image', true);
		$rw_olx_types_arr = array();

		if( $rw_olx_types != null ){
			foreach($rw_olx_types as $type) {
				$rw_olx_types_arr[] = $type->name;
			}
		}
	?>

	<?php if( $rw_olx_image ): ?>
		<div class="rw_olx--image" style="background-image: url(<?php echo $rw_olx_image; ?>);"></div>
	<?php endif; ?>

	<div class="rw_olx--content">
		<div class="rw_olx--title">
			<?php the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
		</div>
		<?php if( $rw_olx_types != null && !empty($rw_olx_types_arr) ): ?>
			<div class="rw_olx--meta">
				<?php _e( 'Тип публікації: ', 'twentytwentyonechild' ); ?><?php echo implode(", ", $rw_olx_types_arr); ?>		
			</div>
		<?php endif; ?>
		<div class="rw_olx--descr">
			<?php echo custom_excerpt(13, '...'); ?>
		</div>
		<div class="rw_olx--readmore">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php _e( 'Читати більше', 'twentytwentyonechild' ); ?></a>
		</div>
	</div>

</article>