<?php  

// Add theme scripts and style
add_action( 'wp_enqueue_scripts', 'twentytwentyfone_child_scripts_styles' );
function twentytwentyfone_child_scripts_styles() {
	wp_enqueue_style( 'twentytwentyfone-child-css', get_stylesheet_directory_uri() . '/style.css', '', '1.0.0' );

    $localize_params = array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
        'ajax_nonce' => wp_create_nonce('check_nonce')
    ) ;

    wp_register_script('twentytwentyfone-child-script', get_stylesheet_directory_uri() . '/assets/js/theme.js', array('jquery'), '', true);
    wp_localize_script( 'twentytwentyfone-child-script', 'params', $localize_params );
    wp_enqueue_script( 'twentytwentyfone-child-script' );
}

// Add admin scripts and style
add_action( 'admin_enqueue_scripts', 'twentytwentyfone_child_custom_image_upload' );
function twentytwentyfone_child_custom_image_upload(){
	wp_enqueue_script('twentytwentyfone-child-upload-media-js', get_stylesheet_directory_uri() . '/assets/js/upload-meta-media.js', '', false, true);
	wp_enqueue_style( 'twentytwentyfone-child-upload-media-css', get_stylesheet_directory_uri() . '/assets/css/upload-meta-media.css', '', '1.0.0' );
}

?>