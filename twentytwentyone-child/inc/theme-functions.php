<?php  
		
	// Add custom metabox
	add_action('add_meta_boxes', 'add_custom_image_rw_olx');
	function add_custom_image_rw_olx(){
        add_meta_box(
            'rw_olx_image',
            __('Зображення Публікації','twentytwentyonechild'),
            'rw_olx_image_html',
            'rw_olx',
            'side',
            'default'
        );
    }

    // Get image matabox html
    function rw_olx_image_html($post){
        $meta = get_post_meta($post->ID, 'rw_olx_image', true);
    	?>
	        <div class="twentytwentyonechild-uploader">
		        <div class="image-preview" <?php if( !$meta ): ?> style="display: none;" <?php endif; ?>><img src="<?php echo $meta ?>"></div>
	            <input type="hidden" name="rw_olx_image" id="rw_olx_image" class="meta-image" value="<?php echo $meta ?>">
	            <div class="buttons-holder">
		            <input type="button" class="button button-primary image-custom-upload" value="<?php _e('Обрати','twentytwentyonechild'); ?>">
		            <a href="#" class="image-delete image-custom-delete" <?php if( !$meta ): ?> style="display: none;" <?php endif; ?>>
		            	<?php _e('Видалити','twentytwentyonechild'); ?>
		            </a>
	            </div>
	        </div>
    	<?php
    }

    // Save custom post meta
    add_action('save_post', 'save_custom_image_rw_olx');
    function save_custom_image_rw_olx($post_id){
       	if( isset($_POST['rw_olx_image']) ){
        	update_post_meta( $post_id, 'rw_olx_image', $_POST['rw_olx_image'] );
       	}
    }

    // Add Custom excerpt
    function custom_excerpt($limit = null, $separator = null) {
	    if( is_null($limit) ){
	        $excerpt = explode(' ', get_the_excerpt(), '15');
	    }else{
	        $excerpt = explode(' ', get_the_excerpt(), $limit);
	    }
	    if( is_null($separator) ){
	        $separator = '...';
	    }
	    if( count($excerpt) >= $limit ){
	        array_pop($excerpt);
	        $excerpt = implode(" ",$excerpt).$separator;
	    }else{
	        $excerpt = implode(" ",$excerpt);
	    }
	    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);

	    return $excerpt;
	}

	// Load more posts rw_olx
	add_action('wp_ajax_loadmore_rw_olx', 'loadmore_rw_olx');
	add_action('wp_ajax_nopriv_loadmore_rw_olx', 'loadmore_rw_olx');
	function loadmore_rw_olx(){

		check_ajax_referer('check_nonce', 'security') ;

		if( isset( $_POST['maxPage'] ) && isset( $_POST['currentPage'] ) ){

			$rw_olx_arr = array(
                'post_type' => 'rw_olx',
                'posts_per_page' => get_option( 'posts_per_page' ),
                'paged' => absint( $_POST['currentPage'] ) + 1
            );

            $rw_olx = new WP_Query($rw_olx_arr);

            if( $rw_olx->have_posts() ){

            	// Start buffering
            	ob_start();

            	while ( $rw_olx->have_posts() ) { $rw_olx->the_post();
            		get_template_part( 'blocks/rw-olx-item' );
            	}

            	$rw_olx_out = ob_get_clean();
            	echo $rw_olx_out;
            }
            
            // Clean query
            wp_reset_query();
		}
		die;
	}
	
	// Send to admin and author info when new post rw_olx created
	// Note. Be sure that you server has PHPmail.
	add_action( 'transition_post_status', 'email_inform_admin_about_rw_olx', 10, 3 );
	function email_inform_admin_about_rw_olx( $new_status, $old_status, $post ) {
		// Also check if mail() exists
	    if( $post->post_type === 'rw_olx' && $new_status === 'publish' && $old_status !== 'publish' && function_exists( 'mail' ) ){

	    	$authorID = get_post_field('post_author', $post->ID);
	    	$authorEmail = get_the_author_meta('user_email', $authorID);

	    	$mailTo = get_option( 'admin_email' );
	    	$subject = 'Новий пост в Публікаціях';
	    	$message = 'Переглянути новий пост можна тут: ' . get_permalink( $post->ID );

	    	mail( $mailTo, $subject, $message );

	    	// Add Event to send author email after 20min
	    	wp_schedule_single_event( time() + 1200, 'send_custom_mail_to_author', array( $authorEmail ) );

	    }

	}

	add_action( 'send_custom_mail_to_author', 'custom_mail_to_author', 10, 3 );
	function custom_mail_to_author( $authorEmail ) {
		$mailTo = $authorEmail;
    	$subject = 'Новий пост на сайті';
    	$message = 'Ви опублікували новий пост на сайті.';

    	mail( $mailTo, $subject, $message );
	}


?>