<?php  

//Custom CPT
add_action( 'init', 'register_theme_cpt', 0 );
function register_theme_cpt() {

    $labels = array(
        'name'                => __( 'Публікації', 'twentytwentyonechild' ),
        'singular_name'       => __( 'Публікація', 'twentytwentyonechild' ),
        'menu_name'           => __( 'Публікації', 'twentytwentyonechild' ),
        'parent_item_colon'   => __( 'Батьківська Публікація', 'twentytwentyonechild' ),
        'all_items'           => __( 'Всі Публікації', 'twentytwentyonechild' ),
        'view_item'           => __( 'Переглянути Публікацію', 'twentytwentyonechild' ),
        'add_new_item'        => __( 'Створити Нову Публікацію', 'twentytwentyonechild' ),
        'add_new'             => __( 'Створити Нову', 'twentytwentyonechild' ),
        'edit_item'           => __( 'Редагувати Публікацію', 'twentytwentyonechild' ),
        'update_item'         => __( 'Оновити Публікація', 'twentytwentyonechild' ),
        'search_items'        => __( 'Пошук Публікації', 'twentytwentyonechild' ),
        'not_found'           => __( 'Не знайдено', 'twentytwentyonechild' ),
        'not_found_in_trash'  => __( 'Не знайдено в корзині', 'twentytwentyonechild' ),
    );
     
    $args = array(
        'label'               => __( 'rw_olx', 'twentytwentyonechild' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'author'),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_icon'           => 'dashicons-welcome-write-blog',
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'rw_olx', $args );

}

// Custom taxonomies
add_action( 'init', 'register_theme_taxonomies', 0 );
function register_theme_taxonomies(){

	$labels = array(
        'name'                => __( 'Типи Публікацій', 'twentytwentyonechild' ),
        'singular_name'       => __( 'Тип Публікації', 'twentytwentyonechild' ),
        'menu_name'           => __( 'Типи Публікацій', 'twentytwentyonechild' ),
        'all_items'           => __( 'Всі Типи Публікацій', 'twentytwentyonechild' ),
        'parent_item'         => __( 'Батьківський Тип Публікації', 'twentytwentyonechild' ),
        'parent_item_colon'   => __( 'Батьківський Тип Публікації', 'twentytwentyonechild' ),
        'new_item_name'       => __( 'Новий Тип Публікації', 'twentytwentyonechild' ),
        'add_new_item'        => __( 'Створити Новий Тип Публікації', 'twentytwentyonechild' ),
        'edit_item'           => __( 'Редагувати Тип Публікації', 'twentytwentyonechild' ),
        'update_item'         => __( 'Оновити Тип Публікації', 'twentytwentyonechild' ),
        'add_or_remove_items' => __( 'Додати або Видалити Тип Публікації', 'twentytwentyonechild' )
    );
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud'     => true,
        'show_in_rest'      => true,
    );
    register_taxonomy( 'rw_olx_type', 'rw_olx', $args );

}

?>