<?php

// Styles and scripts
include( get_stylesheet_directory() . '/inc/scripts.php' );

// Custom post types and custom taxonomies
include( get_stylesheet_directory() . '/inc/cpt-and-tax.php' );

// Theme functions
include( get_stylesheet_directory() . '/inc/theme-functions.php' );

?>