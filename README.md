# RedWerk Test Task for Andrew Bykovsky

## Requirements 

1) Set up WordPress to any of free hostings
2) Set up any WordPress theme
3) Create child theme
4) Create custom post type "Публікація" ( slug: rw_olx ). Included title, custom image.
5) Create custom taxonomy "Тип публікації" for custom post type "Публікація"
6) Create UI for custom post type. Elements width on desktop - 25%, mobile - 100%. Image always in 16:9 proportions. Title max in 2 rows.
7) Send Email to admin when new post is created. Send Email to author after 20 min.

## Notes from Developer

I set up site on free hosting.

Site is available on : https://bykovsky.000webhostapp.com/

Page for custom post type avalable on : https://bykovsky.000webhostapp.com/rw_olx

Also I have added ajax loadmore for custom post type.

About sending Email to admin and author. Free hosting acount doesn't allow to send emails.

## WP admin credentials

Login: a.bykovsky2517@gmail.com

Password: wx-sAD):t6-z;A2

